const fetch = require('node-fetch'); // Se importa fetch de la librería node-fetch

const getRandomJoke = async () => {  // Definimos funciones asíncronas para poder utilizar el await
  const response = await fetch('http://api.icndb.com/jokes/random/'); // Utilizamos fetch y almacenamos la respuesta
  const data = await response.json(); // Parseamos la respuesta JSON

  console.log("Chiste aleatorio:");
  console.log(data.value.joke); // Imprimimos en consola el chiste
};

const getSomeRandomJoke = async (numJokes) => { // Nuevamente definimos una función asincrona, esta vez para el retorno de multiples chistes
  const response = await fetch(`http://api.icndb.com/jokes/random/${numJokes}`); // Llamamos la API
  const data = await response.json(); // Parseamos la respuesta
  
  console.log(`${numJokes} aleatorios:`);
  data.value.forEach((element, idx) => { // Recorremos el arreglo por cada elemento con forEach
    console.log(`- ${idx}: ${element.joke}`); // Imprimimos en consola el chiste y su índice en el arreglo
  });
};

getRandomJoke(); // Ejecutamos la función que devuelve 1 chiste aleatorio
getSomeRandomJoke(5); // Ejecutamos la función a la que pasamos por parámetros cuantos chistes queremos
