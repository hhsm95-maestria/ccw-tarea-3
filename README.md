# Tarea 3: Creación de una aplicación web

## Información general

* **Fecha:** 11/07/2021
* **Alumno:** Héctor Hugo Sandoval Marcelo
* **Asignatura:** Computación en el Cliente Web
* **Repositorio:** [https://gitlab.com/hhsm95-maestria/ccw-tarea-3](https://gitlab.com/hhsm95-maestria/ccw-tarea-3)

## Objetivo

Probando distintas formas de realizar peticiones http en Javascript. Desde el objeto XMLHttpRequest hasta Web Components.

## Instrucciones

Se recomienda primero tener instalado:

* [Git](https://git-scm.com/downloads): Control de Versiones. Nos ayudará a clonar el repositorio del proyecto
* [VS Code](https://code.visualstudio.com/download): Editor de código profesional. Será más sencillo trabajar con el código
* [Node.js con npm](https://nodejs.org/es/download/). Gestor de paquetes y ejecución de JavaScript sin necesidad de navegador web. Será indispensable para instalar los paquetes que utiliza este proyecto, así como ejecutar un script en JavaScript

Instrucciones:

1. Clonar el repositorio con el comando `git clone https://gitlab.com/hhsm95-maestria/ccw-tarea-3.git`
2. Abrir la carpeta creada `ccw-tarea-3` con VS Code.
3. Abrir una terminal en la ruta raíz de la carpeta y ejecutar el comando `make install`. Este comando instalará las dependencias de `npm` así como los componentes de `bower`
4. Para iniciar el servidor web basta con ejecutar el comando `make start`. Iniciará en la dirección [http://127.0.0.1:8080/](http://127.0.0.1:8080/). La consola confirmará esto, en caso de que el puerto este ocupado se utilizará el siguiente próximo disponible, en este caso el 8081

Con esto ya estaremos listos para probar los ejercicios.

## Ejercicios

Recordemos el objetivo de este trabajo el cual es probar las distintas formas de realizar peticiones http con JavaScript a través del tiempo.

Llamaremos a la API de ICNDB consultando por un chiste aleatorio con la URL `http://api.icndb.com/jokes/random/` a través del método GET, respondiendo en formato `JSON`.

### Chuck 2005

El primer ejercicio es probando la clase `XMLHttpRequest` con el cual se hacían en ese entonces las solicitudes http.

```js
xmlhttp = new XMLHttpRequest(); // Instanciamos un objeto de XMLHttpRequest
xmlhttp.open('GET', 'http://api.icndb.com/jokes/random/', true); // Preparamos la solicitud
xmlhttp.onreadystatechange = function() { // Asignamos una función para que el objeto sepa qué hacer cuando se tenga una respuesta
  var textoChiste = JSON.parse(this.response).value.joke; // Parseamos la respuesta JSON y tomamos el contenido del chiste
  console.log('chiste recibido: ' + textoChiste); // Lo mostramos en consola
  var h1s = document.getElementsByTagName('h1'); // Tomamos los elementos h1 que existan el documento HTML
  h1s[0].innerHTML = textoChiste; // Tomamos el primer elemento H1 y le reemplazamos su contenido HTML por el chiste que recibimos.
}
xmlhttp.send(); // Realiza la petición
```

Captura del funcionamiento, se utiliza [Bootstrap 5](https://getbootstrap.com/) para estilizar el sitio:
![Chuck 2005](assets/chuck-2005.png "Chuck 2005")

Para probar este ejercicio puede hacerse en la ruta: [http://127.0.0.1:8080/chuck2005.html](http://127.0.0.1:8080/chuck2005.html)

### Chuck 2006

El año 2006 surgió una versión temprana de lo que mucho después se convertirá un framework bastante popular: jQuery. Este framework incluyó la función `$.get` la cual permite programar la petición http de una forma más sencilla.

```js
$(function() { // Primero nos aseguramos que todo el contenido HTML haya sido cargado
  // Se utiliza la función $.get que solo necesita la URL, y una función que se ejecutará si la respuesta fue exitosa (estatus http 200)
  $.get("http://api.icndb.com/jokes/random", (response) => {
    var textoChiste = response.value.joke; // jQuery parsea de forma automática la respuesta a JSON debido a que detecta los headers de respuesta y a través del header content-type determina que la respuesta en JSON
    $('h1').text(textoChiste); // Se utiliza el selector de elementos HTML de jQuery para seleccionar todos los h1, y el método text para asignar el chiste en el contenido del h1.
  });
});
```

Captura del funcionamiento, se utiliza [Bootstrap 5](https://getbootstrap.com/) para estilizar el sitio:
![Chuck 2006](assets/chuck-2006.png "Chuck 2006")

Para probar este ejercicio puede hacerse en la ruta: [http://127.0.0.1:8080/chuck2006.html](http://127.0.0.1:8080/chuck2006.html)

### Chuck 2006 con Plugin

jQuery también dio oportunidad a la comunidad de crear sus propios plugins con los cuales era muy sencillo para los desarrolladores extender la funcionalidad de jQuery. En este ejercicio se prueba el plugin de los chistes de Chuck.

Se adjunta la respuesta a la pregunta:

> ¿Cómo se escribían las funciones en las versiones de ECMAScript previas a la versión 6?
Re: Se reemplazan las arrows functions por funciones tradicionales.

Primero se agrega el script que llama al plugin:

```html
<script src="http://code.icndb.com/jquery.icndb.min.js"></script>
```

Después lo podemos utilizar en JavaScript:

```js
$(function() {
  // Se utiliza el plugin, solo debemos pasarle una función para manipular la respuesta
  $.icndb.getRandomJoke((response) => {
    var textoChiste = response.joke; // Tomamos el chiste
    $('h1').text(textoChiste); // Con jQuery lo mostramos en el h1
  });

  // ECMAScript >= 6
  /*
  $.icndb.getRandomJokes({ number: 10, success: (response) => {
    response.forEach(element => {
      $("ul").append('<li class="list-group-item">' + element.joke + '</li>');
    });
  }});
  */

  // ECMAScript < 6
  // Se utiliza otra función del plugin para traer múltiples chistes
  $.icndb.getRandomJokes({ number: 10, success: function(response) {
    // En este caso al ser varios chistes debemos iterar por cada uno de ellos con la función forEach
    response.forEach(function (element) {
      // Utilizamos jQuery para crear nuevos elementos HTML para crear una lista con los chistes
      $("ul").append(`<li class="list-group-item">${element.joke}</li>`);
    });
  }});
});
```

Captura del funcionamiento, se utiliza [Bootstrap 5](https://getbootstrap.com/) para estilizar el sitio:
![Chuck 2006 con Plugin](assets/chuck-2006-plugin.png "Chuck 2006 con Plugin")

Para probar este ejercicio puede hacerse en la ruta: [http://127.0.0.1:8080/chuck2006-plugin.html](http://127.0.0.1:8080/chuck2006-plugin.html)

### Chuck 2014 con Fetch

La función `fetch` fue creada como una alternativa más sencilla de implementar a la clase `XMLHttpRequest`.

Se adjunta la respuesta a la pregunta:
> ¿Qué es el WHATWG? (Organismo que, entre otras cosas, ha definido la API del método fetch).
Re: Su nombre son las siglas de Web Hypertext Application Technology Working Group, en español podría traducirse como
"Grupo de trabajo de tecnología de aplicaciones de hipertexto web". Su lema es "Manteniendo y evolucionando HTML desde el 2004"
y partiendo de esto, es un grupo de personas que se interesan por mejorar la Web con la aplicación de estándares y pruebas.
Fue fundado por empleados de Apple, Mozilla, y Opera en 2004 después de un taller de la W3C, quienes en ese entonces
estaban preocupados por el camino que estaban tomando las tecnologías web y que tenían poco interés en ayudar a los
desarrolladores.

```js
// fetch recibe por parámetro la URL y utiliza por defecto el método GET y utiliza promesas para manejar las respuestas
fetch('http://api.icndb.com/jokes/random/')
  .then(response => response.json()) // Al recibir la respuesta parseamos el JSON
  .then(data => { // Ahora con el JSON parseado podemos manipular la respuesta
    const textoChiste = data.value.joke; // Obtenemos el chiste
    const h1s = document.getElementsByTagName('h1'); // Tomamos los elementos h1
    h1s[0].innerHTML = textoChiste; // Lo asignamos al contenido del primer h1
  });

// Realizamos la segunda consulta para múltiples chistes
fetch('http://api.icndb.com/jokes/random/10')
.then(response => response.json()) // Nuevamente parseamos la respuesta
.then(data => {
  const ul = document.getElementsByTagName('ul')[0]; // Seleccionamos el elemento ul donde crearemos la lista
  data.value.forEach((element) => { // Recorremos la lista de chistes que recibimos
    const li = document.createElement("li"); // Creamos un elemento de lista
    li.innerHTML = element.joke; // Le agregamos el contenido del chiste
    li.classList.add("list-group-item"); // Le aplicamos la clase de estilo de bootstrap
    ul.appendChild(li); // Lo adjuntamos a la lista general
  });
});
```

Captura del funcionamiento, se utiliza [Bootstrap 5](https://getbootstrap.com/) para estilizar el sitio:
![Chuck 2014 con Fetch](assets/chuck-2014-fetch.png "Chuck 2014 con Fetch")

Para probar este ejercicio puede hacerse en la ruta: [http://127.0.0.1:8080/chuck2014-fetch.html](http://127.0.0.1:8080/chuck2014-fetch.html)

### Chuck 2014 con Node Fetch

Node.js al ser una implementación de JavaScript para ser ejecutado fuera del navegador no se actualiza en paralelo con la mencionada versión. Cuando `fetch` apareció se hizo una implementación para Node.js conocida como `node-fetch` que realiza las funcionalidades de `fetch` pero sin la necesidad de un navegador.

```js
const fetch = require('node-fetch'); // Se importa fetch de la librería node-fetch

const getRandomJoke = async () => {  // Definimos funciones asíncronas para poder utilizar el await
  const response = await fetch('http://api.icndb.com/jokes/random/'); // Utilizamos fetch y almacenamos la respuesta
  const data = await response.json(); // Parseamos la respuesta JSON

  console.log("Chiste aleatorio:");
  console.log(data.value.joke); // Imprimimos en consola el chiste
};

const getSomeRandomJoke = async (numJokes) => { // Nuevamente definimos una función asincrona, esta vez para el retorno de multiples chistes
  const response = await fetch(`http://api.icndb.com/jokes/random/${numJokes}`); // Llamamos la API
  const data = await response.json(); // Parseamos la respuesta

  console.log(`${numJokes} aleatorios:`);
  data.value.forEach((element, idx) => { // Recorremos el arreglo por cada elemento con forEach
    console.log(`- ${idx}: ${element.joke}`); // Imprimimos en consola el chiste y su índice en el arreglo
  });
};

getRandomJoke(); // Ejecutamos la función que devuelve 1 chiste aleatorio
getSomeRandomJoke(5); // Ejecutamos la función a la que pasamos por parámetros cuantos chistes queremos
```

Captura del funcionamiento:
![Chuck 2014 con Node Fetch](assets/chuck-2014-node-fetch.png "Chuck 2014 con Node Fetch")

Para probar este ejercicio debe ejecutarse el comando: `node chuck-2014-node-fetch.js`

### Chuck Web Components

Los Web Components surgen como una propuesta a solucionar el problema de dificultad con la reusabilidad de código HTML. El presente ejercicio hace uso de ellos para presentar los chistes de Chuck.

Primero importaremos en la etiqueta `<head>` un polyfill para compatibilidad y el componente de los chistes.

```html
<head>
  <!-- Polyfill para compatibilidad de los Web Components con los navegadores -->
  <script src="./bower_components/webcomponentsjs/webcomponents-lite.min.js">
  </script>
  <!-- Importamos el Web Component de los chistes de Chuck -->
  <link
    rel="import"
    href="./bower_components/chuck-norris-joke/chuck-norris-joke.html"
  />
</head>
```

Una vez hecho esto ya podemos utilizar el Web Component en nuestra página, este caso los colocamos dentro de las celdas de una tabla:

```html
<tr>
  <td>1</td>
  <td><chuck-norris-joke></chuck-norris-joke></td>
</tr>
<tr>
  <td>2</td>
  <td><chuck-norris-joke></chuck-norris-joke></td>
</tr>
<tr>
  <td>3</td>
  <td><chuck-norris-joke></chuck-norris-joke></td>
</tr>
<tr>
```

Captura del funcionamiento, se utiliza [Skeleton](http://getskeleton.com/) para estilizar el sitio:
![Chuck con Web Components](assets/chuck-web-components.png "Chuck con Web Components")

Para probar este ejercicio puede hacerse en la ruta: [http://127.0.0.1:8080/chuck-web-components.html](http://127.0.0.1:8080/chuck-web-components.html)
