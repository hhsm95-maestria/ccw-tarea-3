bower:
	@echo "Instalación de componentes de Bower"
	bower install

npm:
	@echo "Instalación de dependencias NPM"
	npm install

install: npm bower

start:
	@echo "Enciende el servidor web"
	npm run http-server
